class CreateUserProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :user_profiles do |t|
      t.references :user, foreign_key: true
      t.date :birth
      t.string :address, limit: 255
      t.string :phone, limit: 40
      t.string :mobile, limit: 80

      t.timestamps
    end
  end
end
