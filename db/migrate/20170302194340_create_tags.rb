class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :title, limit: 40

      t.timestamps
    end
  end
end
