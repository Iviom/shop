class CreateSales < ActiveRecord::Migration[5.0]
  def change
    create_table :sales do |t|
      t.references :user, foreign_key: true
      t.references :user_order, foreign_key: true
      t.references :product, foreign_key: true
      t.decimal :sum, precision: 10, scale: 2
      t.string :address
      t.string :phone
      t.string :mobile
      t.string :payment_type
      t.string :payment_id
      t.string :payer_id
      t.integer :status
      t.timestamps
    end
  end
end
