class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.references :product
      t.string :title, limit: 80
      t.string :file

      t.timestamps
    end
  end
end
