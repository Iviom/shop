class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.references :category, foreign_key: true
      t.string :title, limit: 40

      t.timestamps
    end
  end
end
