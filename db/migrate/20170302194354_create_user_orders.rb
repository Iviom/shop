class CreateUserOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :user_orders do |t|
      t.references :user, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :amount
      t.integer :status

      t.timestamps
    end
    add_index :user_orders, :status
  end
end
