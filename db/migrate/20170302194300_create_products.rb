class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.string :title, limit: 80
      t.string :info, limit: 255
      t.text :descr
      t.decimal :price, precision: 10, scale: 2
      t.integer :amount
      t.text :size

      t.timestamps
    end
  end
end
