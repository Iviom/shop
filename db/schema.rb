# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170315115224) do

  create_table "categories", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "title",       limit: 40
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["category_id"], name: "index_categories_on_category_id"
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "commentable_id"
    t.string   "commentable_type", limit: 20
    t.text     "text"
    t.boolean  "visible"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "images", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "title",      limit: 80
    t.string   "file"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["product_id"], name: "index_images_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "title",       limit: 80
    t.string   "info",        limit: 255
    t.text     "descr"
    t.decimal  "price",                   precision: 10, scale: 2
    t.integer  "amount"
    t.text     "size"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["category_id"], name: "index_products_on_category_id"
  end

  create_table "products_tags", id: false, force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "tag_id",     null: false
    t.index ["product_id", "tag_id"], name: "index_products_tags_on_product_id_and_tag_id"
    t.index ["tag_id", "product_id"], name: "index_products_tags_on_tag_id_and_product_id"
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "user_order_id"
    t.integer  "product_id"
    t.decimal  "sum",           precision: 10, scale: 2
    t.string   "address"
    t.string   "phone"
    t.string   "mobile"
    t.string   "payment_type"
    t.string   "payment_id"
    t.string   "payer_id"
    t.integer  "status"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["product_id"], name: "index_sales_on_product_id"
    t.index ["user_id"], name: "index_sales_on_user_id"
    t.index ["user_order_id"], name: "index_sales_on_user_order_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string   "title",      limit: 40
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "user_orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.integer  "amount"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_user_orders_on_product_id"
    t.index ["status"], name: "index_user_orders_on_status"
    t.index ["user_id"], name: "index_user_orders_on_user_id"
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "birth"
    t.string   "address",    limit: 255
    t.string   "phone",      limit: 40
    t.string   "mobile",     limit: 80
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["user_id"], name: "index_user_profiles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
