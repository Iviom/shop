# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

unless Rails.env == 'test'
  user = CreateAdminService.new.call
  puts 'CREATED ADMIN USER: ' << user.email
end
# Environment variables (ENV['...']) can be set in the file .env file.
#

# Categories
cloth = Category.create! title: 'Cloth'
['For men', 'For women'].each do |title|
  cloth.children.create!(title: title)
end
for_men = Category.find_by_title('For men')
for_men.children.create!(title: 'Sport shoes')
t_shirts_cat = for_men.children.create!(title: 'T shirts')

electronics = Category.create! title: 'Electronics'
['Laptops', 'Computers', 'Tablets', 'Accessories'].each do |title|
  electronics.children.create!(title: title)
end
puts 'Created categories.'

t_shirt1 = t_shirts_cat.products.create!(
  title: 'Fruit of the loom Valueweight 0610360AZ XL',
  info: 'Dark-blue',
  descr: 'Casual style, 100% cotton',
  price: 90,
  amount: 87,
  size: 'L,XL')
file1 = File.open(File.join(Rails.root, '/lib/assets/images/t-shirt dark blue.jpg'))
img1 = t_shirt1.images.create file: file1

t_shirt2 = t_shirts_cat.products.create!(
  title: 'Green T-shirt',
  info: 'Green',
  descr: 'Casual style, 100% cotton',
  price: 65,
  amount: 187,
  size: 'L,XL, XXL')
file2 = File.open(File.join(Rails.root, '/lib/assets/images/t-shirt dark blue.jpg'))
img2 = t_shirt2.images.create file: file2

puts 'Created products.'
