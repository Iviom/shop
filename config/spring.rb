%w(
  .ruby-version
  .rbenv-vars
  tmp/restart.txt
  tmp/caching-dev.txt
  app/payments/*
).each { |path| Spring.watch(path) }
