Rails.application.routes.draw do
  root to: 'categories#index'
  resources :categories, only: [ :show ]
  resources :products, only: [ :show ]
  resources :user_orders, only: [ :index, :create ]
  post '/buy', controller: :sales, action: :create
  delete '/remove_user_order', controller: :user_orders, action: :destroy
  post '/paypal/create/payment',  
    controller: :sales, action: :paypal_create_payment
  post '/paypal/execute/payment',
    controller: :sales, action: :paypal_execute_payment

  get '/checkout', controller: :user_orders, action: :checkout

  namespace :admin do
    resources :users
    root to: "users#index"
  end
  devise_for :users
  resources :users, only: [ :index, :show ]
end
