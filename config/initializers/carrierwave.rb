if Rails.env.test? or Rails.env.cucumber?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
  end
#  config.after(:suite) do
    # Get rid of the linked images
   # if Rails.env.test? || Rails.env.cucumber?
    #  FileUtils.rm_rf(CarrierWave::Uploader::Base.root)
    #end
#  end
end
