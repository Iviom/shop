require 'carrierwave/test/matchers'

describe ImageUploader do
  include CarrierWave::Test::Matchers

  let(:image) { FactoryGirl.create(:image) }
  subject(:uploader) { image.file }

  before do
    ImageUploader.enable_processing = true
  end

  after do
    ImageUploader.enable_processing = false
    uploader.remove!
  end

  context 'the default version' do
    it "scales down an image to fit within 300 by 500 pixels" do
      expect(uploader).to be_no_larger_than(300, 500)
    end
  end

  context 'the small version' do
    it "scales down an image to fit within 200 by 200 pixels" do
      expect(uploader.small).to be_no_larger_than(200, 200)
    end
  end

  it "makes the image readable only to the owner and not executable" do
    expect(uploader).to have_permissions(0600)
  end

  it "has the correct format" do
    expect(uploader).to be_format('jpeg')
  end

end
