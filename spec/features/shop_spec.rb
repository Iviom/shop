feature 'Shop' do

  shared_examples "buyer" do

    before(:each) do
      cloth = FactoryGirl.create :category, title: 'Cloth'
      tshirts = FactoryGirl.create :category, parent: cloth, title: 'T shirts'
      @tshirt = FactoryGirl.create :product, category: tshirts, 
        title: 'Dark blue shirt', price: 87.87
      green_shirt = FactoryGirl.create :product, title: 'Green shirt', price: 12.13, category: tshirts
      FactoryGirl.create :image, product: @tshirt
      FactoryGirl.create :image, product: green_shirt
    end

    scenario 'look at subcategories from category card on home page' do
      visit root_path
      page.first('#category-cards').click_on 'T shirts'
      expect(page.first('#category-title')).to have_content('T shirts')
    end

    scenario 'select low level category with products using catalogue' do
      visit root_path
      expect(page).to have_content 'Catalogue'
      page.first('#catalogue').click_on 'T shirts'
      product_cards = page.first('#product-cards')
      expect(product_cards).to have_content('Dark blue shirt')
      expect(product_cards).to have_css("img[src='#{@tshirt.images.first.file.small.url}']")
    end

    scenario 'add item to cart and remove it from cart' do
      add_item_to_cart
      product_summary = page.first(:xpath, "//div[contains(@class,'product-summary') and descendant::a[text()='Dark blue shirt']]")
      product_summary.click_on 'X'
      expect(page.first('.alert-box.success')).to have_content('Item was removed from cart')
      expect(page.first('#in-cart')).to have_content('In cart: 1')
    end
 
    scenario 'add item to cart and buy it' do
      add_item_to_cart

      expect(page.first('.product-summary')).to have_content('Dark blue shirt')
      page.click_on 'Checkout'
      expect(page).to have_css('input#sale_address')
      expect(page).to have_css('input#sale_phone')
      expect(page).to have_css('input#sale_mobile')
      expect(page.first('#cart-summary')).to have_content('Dark blue shirt')
      expect(page.first('#cart-summary')).to have_content('Green shirt')
      expect(page.first('#checkout-sum')).to have_content('100.0 $')
      page.click_on 'Buy'

      expect(page.first('.alert-box.success')).to have_content('Bought goods on 100.0')
      expect(page.first('#in-cart')).to have_content('In cart: 0')
    end

    def add_item_to_cart
      visit root_path
      expect(page).to have_content 'Catalogue'
      page.first('#catalogue').click_on 'T shirts'
      product_preview = page.first('.product-preview')
      expect(product_preview).to have_content('Dark blue shirt')
      product_preview.click_on 'Add to cart'
      page.click_on 'Add to cart'

      expect(page.first('.alert-box.success')).to have_content('Added to cart')
      expect(page.first('#in-cart')).to have_content('In cart: 2')
      product_preview = page.first('.product-preview')
      expect(product_preview).to have_content('Dark blue shirt')
      product_preview.click_on 'In cart'
    end

  end

  describe 'Signed user' do
    before(:each) do
      FactoryGirl.create :user, email: 'signeduser@test.com', password: 'test_password'
      signin('signeduser@test.com', 'test_password')
    end

    it_behaves_like 'buyer'
  end

  describe 'Visitor' do
    it_behaves_like 'buyer'
  end

end
