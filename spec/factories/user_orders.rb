FactoryGirl.define do
  factory :user_order do
    user { create :user }
    product { create :product }
    amount 54
    status :active
  end
end
