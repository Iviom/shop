FactoryGirl.define do
  factory :sale do
    user { create(:user) }
    user_order { create(:user_order) }
    product { create(:product) }
    sum "9.99"
    address "Kiev, Gagarins Str. 172, a. 87"
    phone "(047) 123-45-67"
    mobile "(095) 765-43-21"
  end
end
