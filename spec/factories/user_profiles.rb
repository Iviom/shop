FactoryGirl.define do
  factory :user_profile do
    user { create(:user) }
    birth "2011-03-02"
    address "MyString"
    phone "MyString"
    mobile "MyString"
  end
end
