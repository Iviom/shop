FactoryGirl.define do
  factory :product do
    category { create(:category) }
    title "MyString"
    info "MyString"
    descr "MyText"
    price "9.99"
    amount 1
    size "MyText"
  end
end
