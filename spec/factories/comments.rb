FactoryGirl.define do
  factory :comment do
    user { create(:user) }
    commentable_id { create(:product).id }
    commentable_type { Product }
    text "MyText"
    visible false
  end
end
