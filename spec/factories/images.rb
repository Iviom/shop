include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :image do
    product { create(:product) }
    title "MyString"
    file { fixture_file_upload(Rails.root.join('spec', 'files', 'boatneck.jpg'), 'image/jpg') }
  end
end
