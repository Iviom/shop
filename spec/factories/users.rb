FactoryGirl.define do
  sequence(:name) { |n| "Test User#{n}" }
  sequence(:email) { |n| "user#{n}@example.com" }

  factory :user do
    confirmed_at Time.now
    name
    email
    password "please12345"

    trait :admin do
      role 'admin'
    end

  end
end
