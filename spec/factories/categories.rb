FactoryGirl.define do
  sequence :title { |n| "Title" + 'a' * n }

  factory :category do
    category_id nil
    title
  end
end
