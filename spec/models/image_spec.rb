require 'rails_helper'

RSpec.describe Image, type: :model do
  it_should_have_actual_schema

  it { should validate_length_of(:title).is_at_most(80) }
end
