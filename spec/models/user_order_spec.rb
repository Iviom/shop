require 'rails_helper'

RSpec.describe UserOrder, type: :model do
  it_should_have_actual_schema

  it { should belong_to :user }
  it { should belong_to :product }
  it { should validate_presence_of :amount }
  it { should validate_numericality_of(:amount)
    .only_integer
    .is_greater_than(0) }
  it { should validate_numericality_of(:amount)
    .is_greater_than(0) }
  it { should define_enum_for(:status).with([ :active, :in_process, :done]) }
end
