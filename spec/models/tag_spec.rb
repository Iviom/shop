require 'rails_helper'

RSpec.describe Tag, type: :model do
  it_should_have_actual_schema

  it { have_and_belong_to_many(:products) }

  it { should validate_presence_of(:title) }
  it { should validate_length_of(:title).is_at_least(1).is_at_most(40) }
end
