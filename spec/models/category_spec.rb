require 'rails_helper'

RSpec.describe Category, type: :model do
  it_should_have_actual_schema

  it { should belong_to :parent }
  it { should have_many :children }
  it { should have_many :products }
  it { should validate_presence_of :title }
  it { should validate_length_of(:title).is_at_least(4).is_at_most(40) }
  it { should validate_uniqueness_of :title }
  it { should allow_value('aaa' + (' ' * 40) + 'a').for(:title) }
  it { should_not allow_values(nil, 'sht','  sht ', 'l' * 41, 'no num 1').for(:title) }
  it "should have scope 'main'" do
    FactoryGirl.create(:category, title: 'Main', category_id: nil)
    Category.main.each do |c|
      expect(c.category_id).to be_nil  
    end
  end
end
