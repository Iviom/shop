require 'rails_helper'

RSpec.describe Sale, type: :model do
  it_should_have_actual_schema

  it { should belong_to(:user) }
  it { should belong_to(:user_order) }
  it { should validate_presence_of(:sum) }
  it { should validate_numericality_of(:sum).is_greater_than(0) }
  it { should allow_values(21, 0.01, 1.1).for(:sum) }
  it { should_not allow_values(-1, 0.001).for(:sum) }
end
