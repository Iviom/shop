describe User do
  it_should_have_actual_schema

  it { should have_one(:profile) }
  it { should have_many(:orders) }
  it { should have_many(:sales) }
  it { should have_many(:comments) }

  it { should define_enum_for(:role).with([:user, :vip, :admin]) }
#  it { should validate_presence_of(:name) }
#  it { should validate_uniqueness_of(:name) }
#  it { should validate_length_of(:name).is_at_least(2).is_at_most(40) }
#  it { should allow_values('Andrew1', 'John Doe').for(:name) }
#  it { should_not allow_values('Serg#', 'Alex_').for(:name) }
end
