require 'rails_helper'

RSpec.describe UserProfile, type: :model do
  it_should_have_actual_schema

  it { should belong_to(:user) }

  it { should allow_values(119.years.ago, 5.years.ago ).for(:birth) }
  it { should_not allow_values(121.years.ago, 3.years.ago).for(:birth) }
  it { should validate_length_of(:address).is_at_most(255) }
  it { should validate_length_of(:phone).is_at_most(20) }
  it { should validate_length_of(:mobile).is_at_most(80) }
end
