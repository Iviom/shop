require 'rails_helper'

RSpec.describe Comment, type: :model do
  it_should_have_actual_schema

  it { should belong_to :user }
  it { should belong_to :commentable }
  it { should validate_presence_of :user }
  it { should validate_presence_of :commentable }
  it { should validate_presence_of :text }
  it { should validate_length_of(:text).is_at_least(1) }
  it { should validate_exclusion_of(:visible).in_array([nil]) }

end
