require 'rails_helper'

RSpec.describe Product, type: :model do
  it_should_have_actual_schema

  it { should belong_to :category }
  it { should have_and_belong_to_many :tags }
  it { should have_many :sales }
  it { should have_many :user_orders }
  it { should have_many :images }
  it { should have_many :comments }

  it { should validate_presence_of :title }
  it { should validate_length_of(:title).is_at_least(2).is_at_most(80) }
  it { should validate_length_of(:info).is_at_most(255) }
  it { should validate_presence_of :price }
  it { should validate_numericality_of(:price).is_greater_than(0) }
  it { should allow_values(21, 0.01, 1.1).for(:price) }
  it { should_not allow_values(-1, 0.001).for(:price) }
  it { should validate_presence_of(:amount) }
  it { should validate_numericality_of(:amount).only_integer.is_greater_than_or_equal_to(0) }
end
