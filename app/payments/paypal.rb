class PaypalPayment
  
  def self.find(paypal_id)
    PayPal::SDK::REST::Payment.find(paypal_id)
  end

  def initialize(products, root_path)
    data = {
      intent: 'sale',
      #"experience_profile_id":"experience_profile_id",
      redirect_urls: { 
        return_url: root_path + '/success', 
        cancel_url: root_path + '/cancel'  
      },
      payer: {
        payment_method: 'paypal'
      },
      transactions: [
        transaction(products)
      ]
    }
    @payment = PayPal::SDK::REST::Payment.new(data)
  end

  def create
    !!@payment.create
  end

  def error
    @payment.error
  end

  def id
    @payment.id
  end

  protected

    def transaction(products)
      {
        reference_id: "#{products.pluck(:id).join(',')}",
        amount: {
          currency: 'USD',
          total: total_price(products).to_s,
          details: {
            "subtotal": subtotal(products).to_s,
            "shipping": shipping(products).to_s,
            "tax": tax(products).to_s,
            "shipping_discount": shipping_discount(products)
          }
        },
        "description": description(products),
        "invoice_number": Time.now.to_s + rand(100000000000).to_s,
        "custom": "merchant custom data",
        item_list: {
          "items": items(products)
        }
      }
    end

    def total_price(products)
      subtotal(products) + shipping(products) + tax(products) - 
        shipping_discount(products)
    end

    def subtotal(products)
      products.pluck(:price).sum
    end

    def shipping(products)
      1
    end

    def tax(products)
      1
    end

    def shipping_discount(products)
      2
    end

    def description(products)
      products.inject("Sale:") { |descr, p| ' ' +  descr + p.title } 
    end

    def items(products)
      products.map do |p|
        {
          "quantity": "1",
          "name": p.title,
          "price": p.price.to_s,
          "currency": "USD",
          "description": p.descr,
          "tax": item_tax(p).to_s
        }
      end
    end

    def item_tax(product)
      0
    end

end
