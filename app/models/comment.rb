class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  validates :user, presence: true
  validates :commentable, presence: true

  validates :text, presence: true, length: { minimum: 1 }
  validates :visible, exclusion: { in: [nil] }

  before_validation { self.text = text.try(:strip) }
end
