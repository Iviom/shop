class UserProfile < ApplicationRecord
  BIRTH_MIN = 120
  BIRTH_MAX = 4

  belongs_to :user

  validates_associated :user

  validates :birth, allow_blank: true, 
    date: { after: BIRTH_MIN.years.ago, before: BIRTH_MAX.years.ago }
  validates :address, length: { maximum: 255 }
  validates :phone, length: { maximum: 20 }
  validates :mobile, length: { maximum: 80 }
end
