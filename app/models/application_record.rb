class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  DECIMAL_REGEX = /\A\d+(?:\.\d{1,2})?\z/
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/
end
