class Tag < ApplicationRecord
  has_and_belongs_to_many :products

  validates :title, presence: true, length: { in: 1..40 }
end
