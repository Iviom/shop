class Image < ApplicationRecord
  mount_uploader :file, ImageUploader

  belongs_to :product

  validates :title, length: { maximum: 80 }
end
