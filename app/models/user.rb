class User < ApplicationRecord
  USER_REGEX = /\A[A-Za-z0-9]+(?: [A-Za-z0-9]+)*\z/

  has_one :profile, class_name: 'UserProfile', dependent: :destroy
  has_many :orders, class_name: UserOrder, dependent: :destroy
  has_many :sales
  has_many :comments, dependent: :destroy

  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :user
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

#  validates :name, presence: true, uniqueness: true, length: { in: 2..40 },
#    format: { with: USER_REGEX }
  validates :email, presence: true, uniqueness: true,
    format: { with: EMAIL_REGEX }
#  validates :password, confirmation: true, length: { in: 10..20 }

  after_create do
    UserProfile.create(user: self)
  end

  before_validation do
    self.email = email.strip.downcase if email
  end

end
