class Category < ApplicationRecord
  belongs_to :parent, class_name: Category, foreign_key: 'category_id',
    optional: true, inverse_of: :children
  has_many :children, class_name: Category, foreign_key: 'category_id',
    inverse_of: :parent
  has_many :products

  scope :main, -> { where(category_id: nil) }
  scope :with_children, -> { includes(:children) }

  validates :title, presence: true, length: { in: 4..40 }, uniqueness: true,
    format: { with: /\A[a-zA-Z ]+\z/, message: "only allows letters and spaces" }

  before_validation { self.title = title.to_s.squish }

end
