class UserOrder < ApplicationRecord
  enum status: [ :active, :in_process, :done ]

  default_scope { where(status: :active) }

  belongs_to :user
  belongs_to :product
  has_one :sale

  validates_associated :product

  validates_uniqueness_of :product_id, scope: [:user_id], 
    conditions: -> { where(status: :active) }
  validates :amount, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :status, inclusion: { in: statuses.keys }

end
