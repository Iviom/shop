class Product < ApplicationRecord
  belongs_to :category
  has_and_belongs_to_many :tags
  has_many :user_orders, dependent: :destroy
  has_many :images, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :sales

  validates :category, presence: true
  validates :title, presence: true, length: { in: 2..80}
  validates :info, length: { maximum: 255 }
  validates :price, presence: true, format: { with: DECIMAL_REGEX }, numericality: { greater_than: 0 }
  validates :amount, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  before_validation do
    self.title = title.try(:strip)
    self.info  = info.try(:strip)
  end

end

