class Sale < ApplicationRecord
  enum status: %w[pending executed]

  belongs_to :user, optional: true
  belongs_to :user_order, optional: true
  belongs_to :product

  validates_associated :user_order

  validates :sum, presence: true, format: { with: DECIMAL_REGEX },
    numericality: { greater_than: 0 }
end
