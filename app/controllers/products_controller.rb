class ProductsController < ApplicationController
  def show
    @product = Product.includes(:images).find_by_id(params[:id])
    @in_cart = user_signed_in? ? 
      current_user.orders.exists?(product_id: @product.id)
      : session[:cart].include?(@product.id)
    redirect_to :root unless @product
  end

  private

    def secure_params
      params.require(:product)
    end
end
