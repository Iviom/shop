class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action { session[:cart] ||= [] }

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  def flash_with_redirect(status, message, options = {})
    options.delete(:errors) unless Rails.env.development?
    if request.xhr?
      render json: { status: status.to_s, message: message }.merge(options)
    else
      redirect_back fallback_location: root_path, 
        (status == :ok ? :notice : :alert) => message
    end
  end

end
