require_dependency Rails.root.join('app/payments/paypal')

class SalesController < ApplicationController
  def create
    if user_signed_in? 
      orders = current_user.orders.includes(:product)
      if orders.empty?
        flash_with_redirect :error, 'No items to buy.'
      else
        sum = 0
        orders.each do |order|
          price = order.product.price
          Sale.create(secure_params.merge({
            user: current_user,
            user_order: order,
            product: order.product,
            sum: price})
          )
          order.status= :done
          order.save
          sum += price
        end
        flash_with_redirect :ok, "Bought goods on #{sum} $"
      end
    else          
      products = Product.where(id: session[:cart]).limit(session[:cart].length)
      products.each do |p|
        p.transactions.create(secure_params.merge({ sum: p.price }))
      end
      session[:cart] = []
      flash_with_redirect :ok, "Bought goods on #{products.pluck(:price).sum}"
    end
  end

  def paypal_create_payment
    payment = PaypalPayment.new(cart_products, root_path)
    if payment.create
      create_sales(secure_params, 'paypal', payment.id)
      render json: { 'paymentID': payment.id }
    else
      render json: { error: payment.error }
    end
  end

  def paypal_execute_payment
    payment = PaypalPayment.find(params['paymentID'])
    binding.pry
    if payment.transactions[0].amount.total.to_f == pending_products.pluck(:price).sum
      if payment.execute( payer_id: params['payerID'] )
        if execute_sales('paypal', params['paymentID'], params['payerID']) &&
          flash[:notice] = "Bought goods on #{payment.transactions[0].amount.total} $"
          render json: { status: 'approved' }
        else
          flash[:alert] = 'Internal error.'
          render json: { status: 'Internal error.' }
        end
      else
        flash[:alert] = payment.error
        render json: { error: payment.error }
      end
    else
      flash[:alert] = 'Wrong sum' 
      render json: { error: 'Wrong sum' }
    end
  end

  private

    def cart_products
      if user_signed_in? 
        Product.where(id: current_user.orders.pluck(:product_id) )
      else          
        Product.where(id: session[:cart]).limit(session[:cart].length)
      end
    end

    def pending_products
      if user_signed_in?
        Product.where(id: current_user.orders.unscope(:where).in_process.pluck(:product_id) )
      else
        Product.where(id: session[:pending]).limit(session[:pending].length)
      end
    end

    def create_sales(data, payment_type, payment_id)
      Sale.pending.where('created_at < ?', 5.minutes.ago).destroy_all

      products = cart_products
      products.each do |p|
        Sale.create secure_params.merge({ 
          status: :pending, 
          sum: p.price, 
          product: p, 
          payment_type: payment_type, 
          payment_id: payment_id,
          user: current_user, 
          address: data[:address], 
          phone: data[:phone], 
          mobile: data[:mobile] })
      end
      if user_signed_in?
        current_user.orders.unscope(:where).in_process.destroy_all
        current_user.orders.update_all(status: :in_process)
      else
        session[:pending] = session[:cart]
        session[:cart] = []
      end
    end

    def execute_sales(payment_type, payment_id, payer_id = nil)
      sales = Sale.where(
        payment_type: payment_type, 
        payment_id: payment_id, 
        product_id: pending_products.ids )
      sales.where(user: current_user) if user_signed_in?
      sales.where(payer_id: payer_id) if payer_id
      Sale.transaction do         
        sales.update_all(status: :executed, payer_id: payer_id)
        if user_signed_in?
          current_user.orders.unscope(:where).in_process.destroy_all
        else
          session[:pending] = []
        end
      end
      true
    end

    def secure_params
      params.require(:sale).permit(:address, :phone, :mobile)
    end
end
