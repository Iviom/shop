class UserOrdersController < ApplicationController
  def index
    product_ids = user_signed_in? ? 
      current_user.orders.map(&:product_id) :
      session[:cart]
    @products = Product.find(product_ids)
  end

  def create
    product = Product.find(secure_params[:product_id])
    if user_signed_in? 
      create_order(product)
    else
      add_to_session_cart(product)
    end
  end

  def destroy
    product_id = secure_params[:product_id].to_i
    if user_signed_in?
      current_user.orders.where(product_id: product_id).limit(1).destroy_all
    else
      session[:cart].delete(product_id) if session[:cart]
    end
    flash_with_redirect(:ok, 'Item was removed from cart.', product_id: product_id)
  rescue ActiveRecord::RecordNotFound
    flash_with_redirect(:error, 'Record not found.', product_id: product_id)
  end

  def checkout
    if user_signed_in? 
      product_ids = current_user.orders.pluck(:product_id)
      @address = current_user.profile.address
      @phone = current_user.profile.phone
      @mobile = current_user.profile.mobile
    else
      product_ids = session[:cart] 
      @address, @phone, @mobile = nil
    end
    @products = Product.where(id: product_ids).limit(product_ids.length)
  end

  private

    def create_order(product)
      order = UserOrder.create( 
        user: current_user,
        product: product,
        amount: 1,
        status: :active
      )
      if order.valid?
        flash_with_redirect(:ok, 'Added to cart')
      else
        flash_with_redirect(:error, "Can't add item to cart", errors: order.errors)
      end
    end
    
    def add_to_session_cart(product)
      (session[:cart] ||= []).push(product.id).uniq!
      flash_with_redirect(:ok, 'Added to cart', product_id: product.id)
    end

    def secure_params
      params.require(:user_order).permit(:product_id)
    end
end
