class CategoriesController < ApplicationController
 
  def index
    @categories = Category.all.order(:category_id)
  end

  def show
    @category = Category.where(id: params[:id])
      .includes(:children).limit(1).first
    if @category
      @products = Product.includes(:images).where(category: @category)
      @cart_product_ids = user_signed_in? ? 
        current_user.orders.pluck(:product_id) : session[:cart] || []
    else 
      redirect_to :root
    end 
  end

  private

    def secure_params
      params.require(:category)
    end
 
end
