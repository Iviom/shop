module ApplicationHelper
  def items_in_cart
    if user_signed_in?
      current_user.orders.count
    else
      session[:cart] ? session[:cart].length : 0
    end
  end
end
